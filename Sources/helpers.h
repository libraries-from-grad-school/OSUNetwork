//
//  helpers.h
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

#ifndef helpers_h
#define helpers_h

#include <stdio.h>

int fcntl2(int32_t, int32_t);
int fcntl3(int32_t, int32_t, int32_t);

int getSockNameStorage(int32_t sock, struct sockaddr_storage *storagePtr, socklen_t *size);

struct sockaddr_storage getSockStorage(const void *data, size_t length);
struct sockaddr_in      getSockIn(     const void *data, size_t length);
struct sockaddr_in6     getSockIn6(    const void *data, size_t length);
struct sockaddr         getSockaddr(   const void *data, size_t length);
struct sockaddr_storage getSockFromIn( struct sockaddr_in);
struct sockaddr_storage getSockFromIn6(struct sockaddr_in6);
struct sockaddr         getSockaddrFromStorage(struct sockaddr_storage storage);

size_t myRecvFrom(int32_t sock, void *bytes, size_t length, void *remoteAddr, socklen_t *remoteAddrLen);
size_t myConnect( int32_t sock, struct sockaddr_storage addr, size_t length);

struct in6_addr isolateIn6Subnet(struct sockaddr_in6);
size_t compareIn6Subnet(struct in6_addr lhs, struct in6_addr rhs);
size_t compareIn6Addr(  struct in6_addr lhs, struct in6_addr rhs);
size_t compareInAddr(   struct in_addr  lhs, struct in_addr  rhs);

#endif /* helpers_h */
