//
//  OSUNetBrowser.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OSULogger

public protocol OSUNetBrowserDelegate {
    func browser(browser: OSUNetBrowser, foundService: NSNetService)
    func browser(browser: OSUNetBrowser, removedService: NSNetService)
}

public class OSUNetBrowser: NSObject {
    private let serviceBrowser = NSNetServiceBrowser()
    public var delegate: OSUNetBrowserDelegate?
    
    public override init() {
        super.init()
        serviceBrowser.delegate = self
        serviceBrowser.scheduleInRunLoop(NSRunLoop.currentRunLoop(),
            forMode: NSRunLoopCommonModes)
    }
    
    public func search(type: String, domain: String = "local.") {
        serviceBrowser.searchForServicesOfType(type, inDomain: domain)
    }
}

extension OSUNetBrowser: NSNetServiceBrowserDelegate {
    public func netServiceBrowser(
        browser: NSNetServiceBrowser,
        didFindDomain domainString: String,
        moreComing: Bool) {
            OSULoggerLog(.Information,
                string: "OSUNetBrowser found domain \(domainString), more coming: \(moreComing)")
    }
    
    public func netServiceBrowser(
        browser: NSNetServiceBrowser,
        didFindService service: NSNetService,
        moreComing: Bool) {
            OSULoggerLog(.Information,
                string: "OSUNetBrowser found service \(service), more coming: \(moreComing)")
            
            if let del = delegate { del.browser(self, foundService: service) }
    }

    public func netServiceBrowser(
        browser: NSNetServiceBrowser,
        didRemoveService service: NSNetService,
        moreComing: Bool) {
            OSULoggerLog(.Information,
                string: "OSUNetBrowser removed service \(service), more coming: \(moreComing)")
            
            if let del = delegate { del.browser(self, removedService: service) }
    }
    
    public func netServiceBrowser(browser: NSNetServiceBrowser,
        didNotSearch errorDict: [String : NSNumber]) {
            OSULoggerLog(.Warning, string: "OSUNetBrowser did not start searching: \(errorDict)")
    }
    
    public func netServiceBrowserWillSearch(browser: NSNetServiceBrowser) {
        OSULoggerLog(.Information, string: "OSUNetBrowser will search")
    }
    
    public func netServiceBrowserDidStopSearch(browser: NSNetServiceBrowser) {
        OSULoggerLog(.Information, string: "OSUNetBrowser stopped searching")
    }
}
