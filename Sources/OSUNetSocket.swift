//
//  OSUNetSocket.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OSULogger

enum OSUNetSocketError: ErrorType {
    case KernelError(Int32)
    case IncorrectSocketType
    case ConnectFailed
    case Timeout
}

// FIXME: This should really be a protocol, not a big meaty, complicated class
public class OSUNetSocket {
    public var boundAddress: NetworkAddress? = nil
    public var socketId: Int32 = 0

    // The only reason this is implicitly unwrapped is because we have to have
    // everything initialized if we throw.  It's initialized in init
    var acceptSource: dispatch_source_t! = nil
    let socketQueue = dispatch_queue_create("edu.orst.ceoas.osunetsocket.acceptqueue", DISPATCH_QUEUE_SERIAL)

    public let socketType: SocketType
    var acceptSocket: Bool? = nil

    public enum ShutdownOptions {
        case Read
        case Write
        case ReadWrite
        
        func toRaw() -> Int32 {
            switch self {
            case .Read:  return SHUT_RD
            case .Write: return SHUT_WR
            case .ReadWrite: return SHUT_RDWR
            }
        }
    }
    
    public enum SocketType {
        case Stream
        case Datagram
        case Raw
        case Unknown
        
        func fromRaw(rawValue: Int32) -> SocketType {
            switch rawValue {
            case SOCK_STREAM:
                return .Stream
            case SOCK_DGRAM:
                return .Datagram
            case SOCK_RAW:
                return .Raw
            default:
                return .Unknown
            }
        }
        
        func toRaw() -> Int32 {
            switch self {
            case .Stream:
                return SOCK_STREAM
            case .Datagram:
                return SOCK_DGRAM
            case .Raw:
                return SOCK_RAW
            default:
                return -1
            }
        }
    }
    
    init(type: SocketType = .Stream, family: Int32) throws {
        // Keep track of the type of socket this is
        socketType = type

        // Create the socket for accepting connections
        // By choosing AF_INET6, we should be able to accept connections from
        // both IPv4 and IPv6 clients.
        socketId = socket(family, socketType.toRaw(), 0)
        guard socketId != -1 else {
            acceptSource = nil
            OSULoggerLog(.Error, string: "Unable to create socket.")
            throw OSUNetSocketError.KernelError(errno)
        }
        
        // We DO NOT! want a SIGPIPE when the remote connection closes.
        // opt for a EPIPE instead.
        var opt_yes: Int32 = 1
        setsockopt(socketId, SOL_SOCKET, SO_NOSIGPIPE, &opt_yes, socklen_t(sizeof(Int32)))
    }
    
    /**
    Create a new socket that will listen for new connections of the given type
    while listening at the given bound address.
    - Parameters:
        - address:
        The address that will be used to listen for new connections.  If the
        port number of the address is zero, the kernel will select an unused
        port for this socket.  At the completion of this method, the portnumber
        of the boundAddress ivar will be set with the port number that the
        kernel selected.
        - type:
        The desired type of socket.  For TCP, use the .Stream type, for UDP, use
        the .Datagram type.  Generally, .Raw (or raw sockets) are not supported.
    - Throws: KernelError: This error means that the socket API generated an error
        which set errno.  The value of errno is included in the error.
    */
    convenience init(type: SocketType = .Stream, bindAddress: NetworkAddress) throws {
        do {
            try self.init(type: type, family: bindAddress.family)
        
            // Bind the address
            let addrData = try bindAddress.getNSData()
            let ret = bind(socketId, UnsafePointer<sockaddr>(addrData.bytes), socklen_t(addrData.length))
            guard ret != -1 else {
                OSULoggerLog(.Error,
                    string: "Unable to bind socket address, errno \(errno): " +
                    "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
                throw OSUNetSocketError.KernelError(errno)
            }
            
            // Get the used address from the socket
            boundAddress = try getNetworkAddress(socketId)
            
        } catch let error {
            OSULoggerLog(.Error, string: "Unable to initialize socket")
            throw error
        }
    }
    
    /**
    Attempt to create a socket that is connected to the provided address.  
    If a timeout is provided and the connect hasn't succeeded within that time, 
    then an error will be thrown
    * Parameters:
        * address:
        The address of the remote host that is to be connected to.
        * timeout:
        Amount of time in seconds that we're willing to wait for connection
    * Throws: OSUNetSocketError.Timeout
    */
    convenience init(type: SocketType = .Stream, connectAddress: NetworkAddress, timeout: Double = 0.0) throws {
        let startTime = NSDate()
        var elapsedTime: Double = 0.0

        // If timeout is set to zero, we're ok waiting for a very long time
        var internalTimeout = (timeout == 0.0) ? Double.infinity : timeout
        
        do {
            try self.init(type: type, family: connectAddress.family)
            
            let addrData = try connectAddress.getAddrStructStorage()

            repeat {
                let retval = myConnect(socketId, addrData, sizeof(sockaddr_storage))
                let errorValue = errno
                
                // We're connected!
                if retval == 0 || errorValue == EISCONN {
                    // Create a dispatch source that will trigger on new connections
                    acceptSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, UInt(socketId), 0, socketQueue)

                    // Get the used address from the socket
                    boundAddress = try getNetworkAddress(socketId)

                    acceptSocket = false
                    
                    return
                }
                
                // There are a few errors that mean it's in progress
                // these are fine, we'll keep looping
                else if errorValue != EINPROGRESS && errorValue != EALREADY {
                    throw OSUNetSocketError.KernelError(errorValue)
                }
                
                // If we're still here, we're waiting for a timeout
                // If it's not the last time through the loop, let's add a small
                // delay so we aren't spinning like crazy
                else {
                    elapsedTime = -1 * startTime.timeIntervalSinceNow
                    if elapsedTime < internalTimeout { usleep(1000) }
                }
            } while elapsedTime < internalTimeout
            
            throw OSUNetSocketError.Timeout
        
        } catch let error {
            throw error
        }
    }
    
    /**
    This init method can be used when the user already has a socket from either
    an accept call, socket call, or some other library that provides them a
    socket some other way.
    
    - Parameters:
        - newSocket: 
        This is the socketID used in the socket API for the new socket
        - type:
        This is a reference to the type of socket.  It's really only kept for the
        user, whether they want to keep track of that stuff or not
        - accept:
        This specifies whether this is an accept socket, meaning that this API
        will setup a queue for accepting new connections.  It is assumed that
        the socket has already been bound to an address.
    - Throws: This function will re-throw any error that getNetworkAddress can
    throw as well as any kernel errors that the socket API can produce, setting
    the associated value to the value of errno.
    */
    public init(newSocket: Int32, type: SocketType, accept: Bool = false) throws {
        self.socketId = newSocket
        self.socketType = type
        self.acceptSocket = accept
        
        do {
            boundAddress = try getNetworkAddress(socketId)
        } catch let error {
            boundAddress = IPv4NetworkAddress.anyAddress(0)
            throw error
        }
    }    
    
    /**
    Begin listening on a socket for new connections.  This function only makes
    sense if the socket is of the accept type, meaning it's a socket that's bound
    to a local address and port.
    
    - Parameters:
        - backlog:
        Number of TCP handshakes that are allowed to be in progress, meaning that
        the remote host has sent a SYN packet, but has not been acknowledged by
        this system.  If the number of SYNs sitting in the queue is greater than
        the backlog, the kernel will begin sending RESET packets to the new
        connections.
        - acceptHandler:
        This block is called with the new socket when a new connection is made.
    - Throws: IncorrectSocketType:
        If this method is called on a TCP peer socket, listening makes no sense.
    - Throws: KernelError:
        The socket API call generated a kernel error (errno) that is included in the thrown error.
    
    */
    public func beginListen(backlog: Int, acceptHandler: (newSocket: OSUNetSocket) -> Void) throws {
        // We need to set the socket as non-blocking because although we're
        // receieving notifications when a read will succeed, we don't want to
        // block in the queue preventing other events from being processed.
        do { try nonBlocking(true) }
        catch {
            OSULoggerLog(.Warning,
                string: "Unable to set non-blocking mode on the socket.")
        }

        // This can only be called if accept is true or nil.  It makes no sense
        // to try to listen on a connected socket, for example.
        if acceptSocket == nil { acceptSocket = true }
        // If accept is false, this is a socket that was created to connect, not listen.
        if acceptSocket == false { throw OSUNetSocketError.IncorrectSocketType }
        
        
        // Remember what kind of socket this is
        acceptSocket = true
        
        // Create a dispatch source that will trigger on new connections
        acceptSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, UInt(socketId), 0, socketQueue)

        // Setup the dispatch source handler blocks
        dispatch_source_set_event_handler(acceptSource) {
            // Space for the new socket address
            var newAddr = sockaddr()
            var newAddrLen = socklen_t(sizeof(sockaddr))
            
            // Accept the new connection
            let newSocketID = accept(self.socketId, &newAddr, &newAddrLen)

            // Create the new socket object
            if let newSock = try? OSUNetSocket(newSocket: newSocketID, type: .Stream, accept: false) {
                // Handle this new connection
                acceptHandler(newSocket: newSock)
            }
        }

        dispatch_source_set_cancel_handler(acceptSource) {
            OSULoggerLog(.Information,
                string: "Shutting down socket accept handling")
        }
        
        // Begin dispatch event handling
        dispatch_resume(acceptSource)
        
        // Finally, Begin the listen on the socket
        let ret = listen(socketId, Int32(backlog))

        // Handle errors
        if ret == -1 {
            OSULoggerLog(.Error,
                string: "Failed to start listening on the socket, errno \(errno): " +
                "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
            throw OSUNetSocketError.KernelError(errno)
        }
    }
    
    /**
    Read from the socket until the first of two conditions is met:
    * The requested data has been read
    * The timeout has been reached
    
    The function returns an **NSData** instance that contains the data
    that has been read.
    
    - Parameters:
        - length:
        The requested number of bytes.  If this is equal to Int.Max, then the
        read will continue until the timeout
        - timeout:
        The maximum time that this function will wait for data to be collected.
        A timeout of zero means that the read should block until the requested
        number of bytes have been read.
    
    - Throws: KernelError: The socket API returned an error
    */
    public func readData(length: Int, timeout: Int = 0) throws -> NSData {
        // Because in most cases (huge assumption alert!!) the socket will
        // be set to non-blocking anyway, if there's a timeout, it'll just
        // be a situation where we're polling for a while for all the bytes
        // and the time elapses.
        let startTime = NSDate()
        var timeoutHit = false
        
        var readBytes = 0
        var buffer = [UInt8](count: length, repeatedValue: 0)
        repeat {
            let retval = read(socketId, &buffer[readBytes], length - readBytes)
            guard retval > 0 else {
                if retval < 0 && errno != EINTR {
                    OSULoggerLog(.Warning,
                        string: "Unable to read from socket, errno \(errno): " +
                        "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
                    throw OSUNetSocketError.KernelError(errno)
                }
                continue
            }
            readBytes += retval
            
            if timeout > 0 {
                // Because the start time is prior to now, the interval
                // will be negative.
                if startTime.timeIntervalSinceNow > Double(timeout * -1) {
                    timeoutHit = true
                }
            }
        } while readBytes < length && timeoutHit == false
        
        return NSData(bytes: buffer, length: length)
    }
    
    /**
    Set the non-blocking flag for this socket
    
    - Parameters:
        - value: The desired value of non blocking; true = non-blocking, false = blocking.
    - Throws: KernelError: If the socket API returns an error, this will be
    thrown with the errno as the associated value.
    */
    public func nonBlocking(value: Bool) throws {
        let ret: Int32
        if value == true {
            ret = fcntl3(socketId, F_SETFL, O_NONBLOCK)
        } else {
            ret = fcntl2(socketId, F_SETFL)
        }
        if ret == -1 {
            OSULoggerLog(.Warning,
                string: "Unable to changing blocking behavior on socket, errno \(errno): " +
                "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
            throw OSUNetSocketError.KernelError(errno)
        }
    }
    
    public func writeData(data: NSData) throws {
        var writtenBytes = 0
        let buffer = UnsafePointer<UInt8>(data.bytes)
        repeat {
            let retval = write(socketId,
                buffer.advancedBy(writtenBytes), data.length - writtenBytes)
            guard retval > 0 else {
                if retval < 0 && errno != EINTR {
                    OSULoggerLog(.Warning,
                        string: "Unable to write to socket, errno \(errno): " +
                        "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
                    throw OSUNetSocketError.KernelError(errno)
                }
                continue
            }
            writtenBytes += retval
        } while writtenBytes < data.length
    }
    
    public func writeSourceOnQueue(queue: dispatch_queue_t) -> dispatch_source_t {
        return dispatch_source_create(
            DISPATCH_SOURCE_TYPE_WRITE,
            UInt(socketId), 0, queue)
    }

    public func readSourceOnQueue(queue: dispatch_queue_t) -> dispatch_source_t {
        return dispatch_source_create(
            DISPATCH_SOURCE_TYPE_READ,
            UInt(socketId), 0, queue)
    }
    
    public func receiveFrom(length: Int) throws -> (NSData, NetworkAddress, UInt64) {
        var remoteAddrLen = socklen_t(SOCK_MAXADDRLEN)
        var addrBytes = [UInt8](count: Int(remoteAddrLen), repeatedValue: 0)
        var remoteBytes = [UInt8](count: length, repeatedValue: 0)
        let receievedBytes = myRecvFrom(socketId,
            &remoteBytes, length,
            &addrBytes, &remoteAddrLen)
        let errorValue = errno
        
        // Get a timestamp right away
        let timestamp = mach_absolute_time()
        
        // If there was some error, we just return
        if receievedBytes <= 0 {
            OSULoggerLog(.Warning,
                string: "Unable to read from socket, errno \(errorValue): " +
                "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
            throw OSUNetSocketError.KernelError(errorValue)
        }
        
        // Convert the receieved bytes into an NSData
        let data = NSData(bytes: remoteBytes, length: length)
        
        // Convert the network address
        let remoteAddrData = NSData(bytes: addrBytes, length: Int(remoteAddrLen))
        let addr: NetworkAddress
        do {
            addr = try getNetworkAddress(sockaddr: remoteAddrData)
        } catch let error {
            throw error
        }
        
        return (data, addr, timestamp)
    }
    
    public func sendTo(address: NetworkAddress, data: NSData) throws {
        do {
            let addressData = try address.getNSData()
            let retval = sendto(socketId, data.bytes, data.length, 0,
                UnsafePointer<sockaddr>(addressData.bytes),
                socklen_t(addressData.length))
            
            if retval < 0 {
                OSULoggerLog(.Warning,
                    string: "Unable to sendTo using socket, errno \(errno): " +
                    "\(String(CString: strerror(errno), encoding:NSUTF8StringEncoding)!)")
                throw OSUNetSocketError.KernelError(errno)
            }
        } catch let error {
            throw error
        }
    }

    public func closeSocket() {
        close(socketId)
    }
    
    public func shutdownSocket(option: ShutdownOptions) {
        shutdown(socketId, option.toRaw())
    }
}
