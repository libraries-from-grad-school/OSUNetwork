//
//  OSUNetServer.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OSULogger

let udpEndpointTXTKeyString = "UDPendpoint"
let tcpEndpointTXTKeyString = "TCPendpoint"

enum OSUNetServerError: ErrorType {
    case CouldNotBindToAddress
    case NoSocketsAvailable
}

public protocol OSUNetServerDelegate {
    func new(session: OSUNetSession, server: OSUNetServer) -> Void
    func published(server: OSUNetServer) -> Void
}

extension NSNetServicesError: CustomStringConvertible {
    public var description: String {
        get {
            switch self {
            case .ActivityInProgress:   return "Activity in progress"
            case .BadArgumentError:     return "Bad argument"
            case .CancelledError:       return "Cancelled"
            case .CollisionError:       return "Name collision occured"
            case .InvalidError:         return "Service was improperly configured"
            case .NotFoundError:        return "Net service not found"
            case .TimeoutError:         return "Operation timed out"
            case .UnknownError:         return "Unknown error occured"
            }
        }
    }
}

/**
This function does most of the hard work of un-CoreFoundation-ifying the callback
from the CFSocket infrastructure.  Once it has converted all the types and made
them swift-friendly, it calls the server instance to perform the rest of the work.
*/
internal func TCPServerAcceptCallback(
newSocket: CFSocket!,
type: CFSocketCallBackType,
dataAddress: CFData!,
data: UnsafePointer<Void>,
info: UnsafeMutablePointer<Void>) {
    // We're only interested in accept callback types
    if type != CFSocketCallBackType.AcceptCallBack {
        OSULoggerLog(.Information, string: "Ignoring non-accept callback from OSUNetServer") }

    // Make sure that we can get a reference to the server that's responsible
    let server = UnsafeMutablePointer<OSUNetServer>(info)[0]
    
    // Next, make sure that we were provided an address
    guard let addrData = dataAddress else { return }

    // Then we convert the address data into a NetworkAddress
    guard let address = try? getNetworkAddress(sockaddr: addrData) else { return }

    // Make the data into its true underlying type
    let handle = UnsafeMutablePointer<CFSocketNativeHandle>(data)[0]
    
    // Create a stream pair from the socket.
    // Because this API is unannotated, we have to deal with unmanaged
    // references to everything.  Once we have those references and verify
    // that they are non-nil, we'll take the retained value and use them.
    var unmanagedReadStream:  Unmanaged<CFReadStream>?  = nil
    var unmanagedWriteStream: Unmanaged<CFWriteStream>? = nil
    CFStreamCreatePairWithSocket(kCFAllocatorDefault, handle, &unmanagedReadStream, &unmanagedWriteStream)
    
    if let rStream = unmanagedReadStream, let wStream = unmanagedWriteStream {
        let readStream  = rStream.takeRetainedValue()
        let writeStream = wStream.takeRetainedValue()
        // The stream should close and release the socket when it's released
        CFReadStreamSetProperty(readStream,  kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue)
        CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue)
        // Now, we're all finished processing the arguments, so we can let
        // the OSUNetServer instance know about the new connection.
        server.handleNewConnectionFrom(address, inputStream: readStream, outputStream: writeStream)
    } else {
        close(handle)
        // I'm not actually sure what the apporpriate action is here,
        // But it's not something that can be easily managed.
        OSULoggerLog(.Error, string: "At least one of the streams from a new socket were nil.")
        assert(false, "Something that should never happened, and we don't know what to do. \(#file):\(#line)")
    }

}


public class OSUNetServer: NSObject, NSNetServiceDelegate {
    public var delegate: OSUNetServerDelegate? = nil
    // Domain and name, if blank, will be replaced by the system as apporiate
    public var domain = ""
    public var name: String = ""
    // The type ivar is required for bonjour publication
    public var type: String? = nil
    // The txt record provides metadata for the bonjour service record
    public var txt: [String: NSData]? = nil {
        didSet {
            guard netService != nil else { return }
            if let txtValue = txt {
                netService!.setTXTRecordData(NSNetService.dataFromTXTRecordDictionary(txtValue))
            } else {
                netService!.setTXTRecordData(NSNetService.dataFromTXTRecordDictionary([String: NSData]()))
            }
        }
    }
    
    public var port: Int = 0
    var socket: CFSocket? = nil
    var netService: NSNetService? = nil
    
    private var selfReference: OSUNetServer! = nil
    
    override public var description: String { get {
            return "OSUNetServer; name.domain: \(name).\(domain), port \(port), type \(type), TXT: \n\(txt)"
        }
    }
    
    var _published: Bool = false
    public var published: Bool {
        get {
            return _published
        }
    }
    
    internal func acceptCallbackFrom(host: NetworkAddress,
        netSocket: CFSocket, sockHandle: CFSocketNativeHandle) {
        
    }
    
    public func start(serviceRunLoop: NSRunLoop? = nil) throws {
        // Because I can't get a mutable reference to self, I have to copy a
        // reference to self in a mutable (but otherwise protected) ivar that I
        // can pass to the socket context
        selfReference = self
        
        // Create the socket context for later core foundation socket calls
        var socketContext = CFSocketContext(version: 0, info: &selfReference,
            retain: nil, release: nil, copyDescription: nil)
        socket = CFSocketCreate(
            kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP,
            CFSocketCallBackType.AcceptCallBack.rawValue,
            TCPServerAcceptCallback, &socketContext)
        
        guard socket != nil else {
            OSULoggerLog(.Error, string: "Unable to create new socket")
            throw OSUNetServerError.NoSocketsAvailable
        }
        
        // Allow local address reuse
        var yes: Int = 1
        setsockopt(CFSocketGetNative(socket),
            SOL_SOCKET, SO_REUSEADDR, &yes, socklen_t(sizeof(Int)))
        
        do {
            // Setup the endpoint, if the port is 0, the kernel will select one for us
            let address = IPv4NetworkAddress.anyAddress(port)
            guard CFSocketSetAddress(socket, try address.getNSData()) == .Success else {
                socket = nil
                OSULoggerLog(.Error, string: "Unable to bind address to socket.")
                throw OSUNetServerError.CouldNotBindToAddress
            }

            // If we let the kernel pick a port for us, we need to get it
            if port == 0 {
                let addrData = CFSocketCopyAddress(socket) as CFData
                let addr = try IPv4NetworkAddress(storage: addrData)
                if addr.portNumber != 0 {
                    port = addr.portNumber
                } else {
                    OSULoggerLog(.Error,
                        string: "Unable to get port number from kernel")
                    throw OSUNetServerError.CouldNotBindToAddress
                }
            }
        } catch let error {
            throw error
        }
        
        // Setup the runloop sources
        let cfRunloop = serviceRunLoop?.getCFRunLoop() ?? CFRunLoopGetMain()
        let source = CFSocketCreateRunLoopSource(kCFAllocatorDefault, socket, 0)
        CFRunLoopAddSource(cfRunloop, source, kCFRunLoopCommonModes)
        
        // If a type is provided, publish the service using bonjour
        if type != nil {
            netService = NSNetService(domain: domain, type: type!, name: name, port: Int32(port))
            if txt != nil {
                netService!.setTXTRecordData(NSNetService.dataFromTXTRecordDictionary(txt!))
            } else {
                netService!.setTXTRecordData(NSNetService.dataFromTXTRecordDictionary([String: NSData]()))
            }
            
            if let rl = serviceRunLoop {
                netService!.scheduleInRunLoop(rl, forMode:NSRunLoopCommonModes)
            }
            
            netService!.delegate = self
            netService!.publish()
        }
        
        OSULoggerLog(.Information, string: "Started server on port \(port)")
    }
    
    public func stop() {
        if netService != nil {
            netService!.stop()
            netService = nil
        }
        
        // We need to reset the self reference to nil so that ARC can clean up.
        selfReference = nil
        CFSocketInvalidate(socket)
        socket = nil
    }
    
    func handleNewConnectionFrom(host: NetworkAddress, inputStream: NSInputStream, outputStream: NSOutputStream) {
        // If there isn't a delegate set, there isn't any point to doing work.
        guard let del = delegate else { return }
        // Create a new session for the streams
        let hostName = host.hostName ?? host.address
        let session = OSUNetSession(inputStream: inputStream, outputStream: outputStream, name: hostName)
        OSULoggerLog(OSULogger.Severity.Information, string: "Connected to \(session.name) on port \(host.portNumber)")
        // Let the delegate know that we have a new session
        del.new(session, server: self)
    }

    public func netServiceDidResolveAddress(sender: NSNetService) {
        OSULoggerLog(.Information, string: "Net service resolved address.")
    }
    
    public func netService(sender: NSNetService, didNotResolve errorDict: [String : NSNumber]) {
        if let errorCode = errorDict[NSNetServicesErrorCode],
           let errorEnum = NSNetServicesError(rawValue: errorCode.integerValue) {
            OSULoggerLog(.Warning, string: "Net service did not resolve address, errors: \(errorEnum.description)")
        }
    }
    
    public func netServiceWillResolve(sender: NSNetService) {
        OSULoggerLog(.Information, string: "Net service will resolve")
    }
    
    public func netServiceWillPublish(sender: NSNetService) {
        OSULoggerLog(.Information, string: "Net service will publish")
    }
    
  public   
    func netServiceDidStop(sender: NSNetService) {
        _published = false
        
        OSULoggerLog(.Warning, string: "Net service stopped")
    }
    
    public func netServiceDidPublish(sender: NSNetService) {
        _published = true
        
        // Now that the service is published, the name and domain are valid
        name = sender.name
        domain = sender.domain
        OSULoggerLog(.Information, string: "Service published as \(name).\(domain)")
        guard delegate != nil else { return }
        delegate!.published(self)
    }
    
  public
    func netService(sender: NSNetService, didUpdateTXTRecordData data: NSData) {
        OSULoggerLog(.Information, string: "NetService updated TXT record data: \(data)")
    }
    
  public   
    func netService(sender: NSNetService, didNotPublish errorDict: [String : NSNumber]) {
        if let errorCode = errorDict[NSNetServicesErrorCode],
           let errorEnum = NSNetServicesError(rawValue: errorCode.integerValue) {
        
            OSULoggerLog(.Warning, string: "Net service did not publish: \(errorEnum.description)")
        }
    }
}