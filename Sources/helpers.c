//
//  helpers.c
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

#include "helpers.h"
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>

int
fcntl2(int32_t fd, int32_t cmd) {
    return fcntl(fd, cmd);
}

int
fcntl3(int32_t fd, int32_t cmd, int32_t arg) {
    return fcntl(fd, cmd, arg);
}

int
getSockNameStorage(int32_t sock, struct sockaddr_storage *storagePtr, socklen_t *size) {
    return getsockname(sock, (struct sockaddr *)storagePtr, size);
}

struct sockaddr_storage
getSockStorage(const void *data, size_t length) {
    struct sockaddr_storage retval;
    size_t minLength = (length <= sizeof(struct sockaddr_storage))? length : sizeof(struct sockaddr_storage);
    memccpy(&retval, data, 1, minLength);
    return retval;
}

struct sockaddr_in
getSockIn(const void *data, size_t length) {
    struct sockaddr_in retval;
    size_t minLength = (length <= sizeof(struct sockaddr_in))? length : sizeof(struct sockaddr_in);
    memccpy(&retval, data, 1, minLength);
    return retval;
}

struct sockaddr_in6
getSockIn6(const void *data, size_t length) {
    struct sockaddr_in6 retval;
    size_t minLength = (length <= sizeof(struct sockaddr_in6))? length : sizeof(struct sockaddr_in6);
    memccpy(&retval, data, 1, minLength);
    return retval;
}

struct sockaddr getSockaddr(const void *data, size_t length) {
    struct sockaddr retval;
    size_t minLength = (length <= sizeof(struct sockaddr))? length : sizeof(struct sockaddr);
    memccpy(&retval, data, 1, minLength);
    return retval;
}


struct sockaddr_storage getSockFromIn( struct sockaddr_in in) {
    return getSockStorage(&in, sizeof(struct sockaddr_in));
}

struct sockaddr_storage getSockFromIn6(struct sockaddr_in6 in6) {
    return getSockStorage(&in6, sizeof(struct sockaddr_in6));
}

struct sockaddr getSockaddrFromStorage(struct sockaddr_storage storage) {
    return getSockaddr(&storage, sizeof(struct sockaddr_storage));
}

// IPv6 subnets are the first two 32-bit fields of the IPv6 address.
struct in6_addr isolateIn6Subnet(struct sockaddr_in6 input) {
    input.sin6_addr.__u6_addr.__u6_addr32[2] = 0; // Erase the address information,
    input.sin6_addr.__u6_addr.__u6_addr32[3] = 0; // which is the lower-order 64 bits.
    return input.sin6_addr;
}

size_t compareIn6Subnet(struct in6_addr lhs, struct in6_addr rhs) {
    for (int i = 0; i < 2; i++) {
        int32_t result = lhs.__u6_addr.__u6_addr32[i] - rhs.__u6_addr.__u6_addr32[i];
        if (result != 0) { return (result < 0) ? -1 : 1; }
    }
    return 0;
}

size_t compareIn6Addr(struct in6_addr lhs, struct in6_addr rhs) {
    for (int i = 0; i < 4; i++) {
        int32_t result = lhs.__u6_addr.__u6_addr32[i] - rhs.__u6_addr.__u6_addr32[i];
        if (result != 0) { return (result < 0) ? -1 : 1; }
    }
    return 0;
}

size_t compareInAddr( struct in_addr  lhs, struct in_addr  rhs) {
    int32_t result = lhs.s_addr - rhs.s_addr;
    if (result != 0) { return (result < 0) ? -1 : 1; }
    else return 0;
}

size_t myRecvFrom(int32_t sock, void *bytes, size_t length, void *remoteAddr, socklen_t *remoteAddrLen) {
    return recvfrom(sock, bytes, length, 0, (struct sockaddr *)remoteAddr, remoteAddrLen);
}

size_t myConnect( int32_t sock, struct sockaddr_storage addr, size_t length) {
    socklen_t len = (length < addr.ss_len) ? (socklen_t)length : addr.ss_len;
    return connect(sock, (struct sockaddr *)&addr, len);
}
