//
//  OSUNetSession.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OSULogger

enum OSUNetSessionError: ErrorType {
    case UnableToCreateStream
    case UnableToOpenStream
    case NotReady
    case UnableToSerializeString
}

/**
OSUNetSessionDelegate

Delegate protocol for events relating to the **OSUNetSession** class.

*/
public protocol OSUNetSessionDelegate {
    
/**
This function notifies the delegate that the OSUNetSession was closed.
This is most probably a result of the remote side of the connection requesting
the closure of the session.

- Parameters:
    - session:
    The OSUNetSession instance responsible for the message
*/
    func netSessionClosed(session: OSUNetSession)

/**
This is a departure from the ObjC implemntation.  Swift REALLY doesn't
like optional functions in a protocol.  Therefore, if the session is in
binary mode, the message argument will be an NSData, otherwise it will be
a Sting.  It's up to the user to use conditional down-casting to make
sure it's what they're expecting.

- Parameters:
    - session:
    The OSUNetSession instance responsible for the message
    - receivedMessage:
    The contents of the message from the network.
    It is up to the receiver to determine whether the contents are an NSData
    or a String.  The contents will be determine by the state of the binaryMode
    ivar of the OSUNetSession class instance
*/
    func netSession(session: OSUNetSession, receivedMessage: AnyObject)
}

/**
OSUNetSession

The OSUNetSession class handles the creation and management of a set of
**NSInputStream** and **NSOutputStream** object.  These objects will trigger
events fairly often, and the tend not to have any relation to the way data
went into the stream.  For example, a set of strings may come out the other
side concatinated, or a single, long, string may be broken into several smaller
chunks.

It is possible to set the class into binary mode, which will not attempt to
divide the data coming into the stream in any way.  Whenever a new data event
occurs, a new NSData instance will be created with the contents.  It is up to
the delegate to perform any division or reassembly of the serialized structures.

Synchronization and serialization of method access to this class is guarenteed
by Grand Central Dispatch, and an NSLock.  The user may call any of the accessor
methods from any thread they desire.  Keep in mind, though, that the order of
messages will be set by the order that any competing threads aquire the lock, or
make it into the queue.

Every concievable use of OSUNetSession will set a delegate object.  This object
must implement the **OSUNetSessionDelegate** protocol.
*/
public class OSUNetSession: NSObject, NSStreamDelegate {
    /// This is the name for this connection that is used in debug printing.
    public var name: String = ""
    /// The delegate that will receive notifications relating to the net session
    public var delegate: OSUNetSessionDelegate? = nil

    /**
    The dispatch queue that this library uses to call delegate methods may be
    changed.  There are cases where the user might want these calls to be
    serialized on a queue that they control, or for testing, it may be useful
    to force these calls to happen on a thread that isn't main.
    
    The user should understand that changing this value during operation may
    cause the order of the messages to be executed out of order.  For example,
    if the dispatch queue is changed from a serial queue while there are several
    messages that need to be delivered, it's possible that the newer messages
    could get dispatched before the older ones.
    */
    var delegateDispatchQueue: dispatch_queue_t = dispatch_get_main_queue()
    
    /**
    Binary mode changes the behavior of the OSUNetSession in how it relates
    to the frequency of invocations of the callback as well as whether nulls
    in the byte stream are consumed.
    
    For example, in string mode, (binaryMode == false) strings are sent over the
    stream with trailing nulls.  On the receiving side, those nulls are
    consumed, and the bytes between the nulls are packaged up into a string
    instance.  In binary mode (binaryMode == true) the contents of a new message
    are sent immediately to the delegate without any processing.
    */
    var binaryMode = true

    // Stream instances
    private var inputStream: NSInputStream? = nil
    private var outputStream: NSOutputStream? = nil
    
    // These iVars contain the state of the outputStream
    // this ensures that we send messages only and always
    // when space is available
    private var stateLock = NSLock()
    private var spaceAvailable = false
    private var outgoingByteOffset = 0
    private var outgoingBuffer: NSData? = nil

    // This array contains messages that are yet to be transmitted
    private var outgoingFIFO = [NSData]()
    
    // Input buffer, contains not-yet completed strings
    private var incomingBuffer: NSMutableData? = nil
    
    override init() {
        super.init()
    }
    
    /**
    This *init* method is intended for the cases where the hostname and port
    of a remote system are known, but there has been no other contact.  The
    method will perform a name resolution, open **NSInputStream** and
    **NSOutputStream** instances, and open the streams.
    
    it is generally expected that the client code will immediately assign a
    delegate once this method returns
    
    - Parameters:
        - addressString:
        This is a string that contains the hostname of the remote system.
        It is appropriate to expect that any hostname that would work within
        the context of a name search or IP address would succeed.  For example
        bonjourName.local or 192.168.0.2 or google.com would all work, assuming
        there is a system on your network broadcasting as bonjourName.
        - portNumber:
        Nothing special here, just the port number of the remote service.
        - name:
        This is the name for this connection that is used in debug printing.
    - Throws: OSUNerSessionError.UnableToCreateStream:
        This error is thrown in the cases where the creation of the
        streams failed.
    */
    public init(addressString: String, portNumber: Int, name: String) throws {
        super.init()
        
        self.name = name

        // Create a host with the matching address
        let host = NSHost(address: addressString)

        OSULoggerLog(.Information, string: "Opening OSUNetSession with \(name) at \(host.address!):\(portNumber)")
        
        // Create a stream pair with the given host
        if let name = host.name {
            if #available(OSX 10.10, *) {
                NSStream.getStreamsToHostWithName(name,
                    port: portNumber,
                    inputStream: &inputStream, outputStream: &outputStream)
            } else {
                //TODO: find replacement for getStreamsToHostWithName for 10.9
                assert(false,
                    "Must find replacement for getStreamsToHostWithName for 10.9")
            }
        } else {
            throw OSUNetSessionError.UnableToCreateStream
        }
        
        // Make sure this succeeded
        guard inputStream != nil && outputStream != nil else {
            OSULoggerLog(.Warning,
                string: "Unable to create stream pair for \(name)")
            throw OSUNetSessionError.UnableToCreateStream
        }

        // We've manually ensured that these are not nil
        inputStream!.delegate = self
        outputStream!.delegate = self
        inputStream!.scheduleInRunLoop(NSRunLoop.mainRunLoop(),
            forMode: NSDefaultRunLoopMode)
        outputStream!.scheduleInRunLoop(NSRunLoop.mainRunLoop(),
            forMode: NSDefaultRunLoopMode)
        inputStream!.open()
        outputStream!.open()
    }
    
    /**
    This *init* method is intended for the cases where the hostname and port
    of a remote system are known, but there has been no other contact.  The
    method will perform a name resolution, open **NSInputStream** and
    **NSOutputStream** instances, and open the streams.
    
    it is generally expected that the client code will immediately assign a
    delegate once this method returns
    
    - Parameters:
        - address:
        This is an instance of the NetworkAddress type, and represents the address
        of the remote system.  It is appropriate to expect that any hostname that
        would work within the context of a name search or IP address would succeed.
        For example bonjourName.local or 192.168.0.2 or google.com would all work,
        assuming there is a system on your network broadcasting as bonjourName.
        - portNumber:
        Nothing special here, just the port number of the remote service.
        - name:
        This is the name for this connection that is used in debug printing.
    - Throws: OSUNerSessionError.UnableToCreateStream:
        This error is thrown in the cases where the creation of the
        streams failed.
    */
    public convenience init(
        address: NetworkAddress, portNumber: Int, name: String) throws {
        do { try self.init(addressString: address.address,
            portNumber: portNumber, name: name) }
        catch let error { throw error }
    }
    
    /**
    This *init* method is intended for the cases where a pair of 
    **NSInputStream** and **NSOutputStream** instances already exist.  This will
    be the case when OSUNetServer receives a connection from a remote site, or
    in some cases, bonjour will create these for you.
    
    it is generally expected that the client code will immediately assign a
    delegate once this method returns
    
    The input and output streams will be opened within this function.
    
    - Parameters:
        - inputStream:
        A pre-created instance of a *NSInputStream* that has been generated by
        an OSUServer instance, or in some cases by bonjour.
        - outputStream:
        A pre-created instance of a *NSOutputStream* that has been generated by
        an OSUServer instance, or in some cases by bonjour.
        - name:
        This is the name for this connection that is used in debug printing.
    */
    public init(inputStream: NSInputStream, outputStream: NSOutputStream,
        name: String) {
        super.init()
        
        self.name = name
        self.inputStream = inputStream
        self.outputStream = outputStream
        
        self.inputStream!.delegate = self
        self.outputStream!.delegate = self
        self.inputStream!.scheduleInRunLoop(NSRunLoop.mainRunLoop(),
            forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.mainRunLoop(),
            forMode: NSDefaultRunLoopMode)
        self.inputStream!.open()
        self.outputStream!.open()
    }
    
    deinit {
        if (inputStream != nil) {
            inputStream!.close()
            inputStream!.removeFromRunLoop(NSRunLoop.mainRunLoop(),
                forMode: NSDefaultRunLoopMode)
            inputStream!.delegate = nil
            inputStream = nil
        }
        
        if (outputStream != nil) {
            outputStream!.close()
            outputStream!.removeFromRunLoop(NSRunLoop.mainRunLoop(),
                forMode: NSDefaultRunLoopMode)
            outputStream!.delegate = nil
            outputStream = nil;
        }
    }
    
    internal func processBytes(bytes: UnsafePointer<UInt8>, length: Int) {
        
        // If we're in binary mode, we don't really don't need to do that much
        if binaryMode {
            let data: NSData
//            if incomingBuffer != nil {
//                incomingBuffer!.appendData(NSData(bytes: bytes, length: length))
//                data = incomingBuffer!
//            } else {
                data = NSData(bytes: bytes, length: length)
//            }
            
            if let del = delegate {
                dispatch_async(delegateDispatchQueue, {
                    del.netSession(self, receivedMessage: data)
                })
            }
        }
        
        // Otherwise, string mode requires us to separate the input at the null
        // values, and send discrete invocations of the newString delegate method
        else {
            // If there's anything in the incoming buffer, create a new
            // composite data object that contains all the data.  Then, we'll
            // divide any strings up at the nulls and send them to the delegate
            let data: NSData
            if incomingBuffer != nil {
                incomingBuffer!.appendData(NSData(bytes: bytes, length: length))
                data = incomingBuffer!
            } else {
                data = NSData(bytes: bytes, length: length)
            }

            // Keep track of all the strings in the data by keeping a list of
            // their ranges
            var ranges = [NSRange]()
            var start = 0
            let data_bytes = UnsafePointer<UInt16>(data.bytes)
            let data_length = data.length / 2
            
            for i in 0 ..< data_length {
                if data_bytes[i] == 0 {
                    ranges.append(NSRange(location: start, length: i - start))
                    start = i+1
                }
            }
            
            for range in ranges {
                // Create a new string from the bytes in this range
                // We need to check some basic sanity issues
                guard range.location >= 0 &&
                    range.location + range.length < data_length else {
                    continue
                }

                // The location and length are in 16-bit indexes, we have to 
                // scale them by 2 to get bytes
                let subData = data.subdataWithRange(NSRange(location: range.location * 2, length: range.length * 2))
                if let s = NSString(bytes: subData.bytes, length: subData.length, encoding: NSUTF16StringEncoding) {
                    let string = String(s)
                    if let del = delegate {
                        dispatch_async(delegateDispatchQueue, {
                            del.netSession(self, receivedMessage: string)
                        })
                    }
                }
            }
            
            // If there is any data remaining, we need to save it for next time.
            if start < data_length {
                // The start is relative to 16-bit values, so we need to double the
                // value of the start
                let subData: NSData = data.subdataWithRange(NSRange(location: start * 2, length: data.length - (start * 2)))
                incomingBuffer = subData.mutableCopy() as? NSMutableData
            }
            // If we're not saving to the incoming buffer, we need to make sure
            // that we delete it, because we've already consumed it.
            else {
                incomingBuffer = nil
            }
        }
    }
    
    private func sendDataInner(data: NSData) {
        // The appropriate action depends on whether we are currently transmitting
        // If the buffer is nil, we aren't.  Begin transmitting.
        // This portion of this method must be protected by a lock
        if stateLock.tryLock() == true {
            // Output stream is guarenteed to be non-nil by the caller.
            if outgoingBuffer == nil && outputStream!.streamStatus == NSStreamStatus.Open {

                // Write to network
                let written = outputStream!.write(UnsafePointer<UInt8>(data.bytes), maxLength:data.length)
                switch written {
                case Int.min ..< 0:
                    OSULoggerLog(.Warning, string: "Encountered an error while writing to stream")
                case 0:
                    OSULoggerLog(.Warning, string: "Reached end of stream while writing to network.  This shouldn't be possible.")
                case 1 ..< data.length:
                    outgoingByteOffset = written
                    outgoingBuffer = data
                case data.length:
                    OSULoggerLog(.Warning, string: "Unknown error sending string.")
                default:
                    OSULoggerLog(.Warning, string: "Somehow wrote more data than the buffer.")
                }

                // Otherwise, we need to add this message to the queue.
            } else {
                // Keep the buffer for later
                outgoingFIFO.append(data)
            }
            
            stateLock.unlock()
        } else {
            OSULoggerLog(.Warning, string: "Unable to aquire state lock in sendDataInner, message lost.")
        }
    }
    
    // By keeping this function seperate, we can test to make sure that it behaves
    // correctly using the unit tests.  It's very difficult to test once it goes into
    // the NSInputStream and NSOutputStreams
    internal func processString(string: String) throws -> NSData {
        let nullArray = [UInt16](count: 1, repeatedValue: 0x0000)
        // Get a null-terminated string and store in an NSData (length doesn't include NULL)
        let nsString = string as NSString
        if let data = nsString.dataUsingEncoding(NSUTF16StringEncoding) {
            let mData = data.mutableCopy() as! NSMutableData
            let nullData = NSData(bytes: nullArray, length: 2)
            mData.appendData(nullData)
            return mData
        } else {
            throw OSUNetSessionError.UnableToSerializeString
        }
    }
    
    /** Send a string to the remote host using the network session
    - throws OSUNetSessionError.NotReady:
    The inputStream and outputStreams are not ready to accept data.
    - throws OSUNetSessionError.UnableToSerializeString:
    
    
    */
    public func sendString(string: String) throws {
        guard inputStream != nil && outputStream != nil else { throw OSUNetSessionError.NotReady }
        do {
            let data = try processString(string)
            self.sendDataInner(data)
        } catch let error {
            throw error
        }
    }
    
    /// Send a block of data to the remote host using the network session
    public func sendData(data: NSData) {
        guard inputStream != nil && outputStream != nil else { return }
        self.sendDataInner(data)
    }
}
