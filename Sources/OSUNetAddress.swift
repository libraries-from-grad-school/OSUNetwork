//
//  OSUNetAddress.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OSULogger

#if OSUNETWORK_JSON_SUPPORT
import PMJSON
#endif

enum NetAddressError: ErrorType {
    case InvalidAddress
    case InvalidScope
    case NullAddress
    case WrongFamily
    case KernelError (Int32)
    case NetmaskMissing
}

extension sockaddr_in {
    /** 
    Create a new sockaddr_in from an **NSData** that is storing either a
    **sockaddr_in** or **sockaddr_storage**.  If the contents of the *storage*
    parameter does not contain a struct of the appropriate format, this init
    function with throw an *InvalidAddress* error.
    
    - Parameters:
        - storage:
    An **NSData** that contains the bytes of either a **sockaddr_in** or a 
    **sockaddr_storage** structure that represents a sockaddr_in.
    
    - Throws: InvalidAddress
    This error is thrown if the bytes stored by the *storage* parameter don't
    represent a sockaddr_in structure
    - Throws: WrongFamily
    This error indicates that the storage structure passed to this initalizer
    represents an address that doesn't match.  For example, a **structaddr_in6**
    being sent to **sockaddr_in**
    */
    init(storage: NSData) throws {
        // Ensure that the size of the data is enough for the first two fields,
        // which are both Uint8s.  We won't access any fields past this until
        // the size is found to be consistent and at least as large as the
        // structure we're using.
        guard storage.length >= 2 else { throw NetAddressError.InvalidAddress }
        
        // Get a reference to the bytes stored in *storage*
        // as a sockaddr_storage struct
        let storageIn = getSockStorage(storage.bytes, storage.length)
        
        // The first byte of both the sockaddr_storage and sockaddr_in structs
        // contains the number of bytes stored.  As a basic sanity check, make
        // sure that the size of the data object is atleast this long.
        guard UInt8(storage.length) >= storageIn.ss_len else {
            throw NetAddressError.InvalidAddress
        }
        
        // Ensure that this is supposed to be a sockaddr_in, not a sockaddr_in6
        // or anything else, and that the provided data parameter carries the
        // advertized number of bytes.
        guard storageIn.ss_family == sa_family_t(AF_INET) else {
            throw NetAddressError.WrongFamily
        }
        
        // Check to make sure that the size is at least as large as the 
        // sockaddr_in structure
        guard storage.length >= sizeof(sockaddr_in) else {
            throw NetAddressError.InvalidAddress
        }
        
        // Now that we know that this is a consistently and sufficiently sized
        // structure of the correct family, we can bit cast the bytes
        // into the final structure
        let structIn = getSockIn(storage.bytes, storage.length)
        
        // All that's left to do is copy the fields from the provided structure
        // to the local copies.
        sin_len      = structIn.sin_len
        sin_family   = structIn.sin_family
        sin_addr     = structIn.sin_addr
        sin_port     = structIn.sin_port
        
        // Finally, we have to initialize the zeros because the copiler will
        // complain if we don't.
        sin_zero = (0, 0, 0, 0, 0, 0, 0, 0)
    }
    
    /**
    Create an instance of the sockaddr_in structure from an address and port
    - Parameters:
        - address:
        This address is the 32-bit representation of the IPv4 address.
        It is assumed that it is in the byte ordering of the host.
        - portNumber:
        This is the port number in host byte order.  The port number zero is
        assumed.
    */
    init(address: UInt32, portNumber: Int = 0) {
        // We have to do this, otherwise the compiler complains
        sin_zero   = (0, 0, 0, 0, 0, 0, 0, 0)
        sin_len    = UInt8(sizeof(sockaddr_in))
        sin_family = UInt8(AF_INET)
        sin_port   = CFSwapInt16HostToBig(UInt16(portNumber))
        // We don't have to convert the address to bigEndian because the tools
        // do that for us.
        sin_addr   = in_addr(s_addr: address)
    }
}

extension sockaddr_in6 {
    /**
    Create a new sockaddr_in6 from an **NSData** that is storing either a
    **sockaddr_in6** or **sockaddr_storage**.  If the contents of the *storage*
    parameter does not contain a struct of the appropriate format, this init
    function with throw an *InvalidAddress* error.
    
    - Parameters:
        - storage:
        An **NSData** that contains the bytes of either a **sockaddr_in6** or a
        **sockaddr_storage** structure that represents a sockaddr_in6.
    
    - Throws: InvalidAddress
    This error is thrown if the bytes stored by the *storage* parameter don't
    represent a sockaddr_in6 structure
    - Throws: WrongFamily
    This error indicates that the storage structure passed to this initalizer
    represents an address that doesn't match.  For example, a **structaddr_in**
    being sent to **sockaddr_in6**
    */
    init(storage: NSData) throws {
        // Ensure that the size of the data is enough for the first two fields,
        // which are both Uint8s.  We won't access any fields past this until
        // the size is found to be consistent and at least as large as the
        // structure we're using.
        guard storage.length >= 2 else { throw NetAddressError.InvalidAddress }
        
        // Get a reference to the bytes stored in *storage* as a
        // sockaddr_storage struct
        let storageIn = getSockStorage(storage.bytes, storage.length)
        
        // The first byte of both the sockaddr_storage and sockaddr_in structs
        // contains the number of bytes stored.  As a basic sanity check, make
        // sure that this matches the number of bytes stored by the NSData.
        guard UInt8(storage.length) == storageIn.ss_len else {
            throw NetAddressError.InvalidAddress
        }
        
        // Ensure that this is supposed to be a sockaddr_in6, not a sockaddr_in
        // or anything else
        guard storageIn.ss_family == sa_family_t(AF_INET6) else {
            throw NetAddressError.WrongFamily
        }
        
        // Check to make sure that the size is at least as large as the
        // sockaddr_in structure
        guard storage.length >= sizeof(sockaddr_in6) else {
            throw NetAddressError.InvalidAddress
        }
        
        // Now that we know that this is a consistently and sufficiently sized
        // structure of the correct family, we can bit cast the bytes
        // into the final structure
        let structIn = getSockIn6(storage.bytes, storage.length)
        
        // All that's left to do is copy the fields from the provided structure
        // to the local copies.
        sin6_len      = structIn.sin6_len
        sin6_family   = structIn.sin6_family
        sin6_port     = structIn.sin6_port
        sin6_flowinfo = structIn.sin6_flowinfo
        sin6_addr     = structIn.sin6_addr
        sin6_scope_id = structIn.sin6_scope_id
    }
    
    /**
    Create an instance of the sockaddr_in6 structure from an address and port
    - Parameters:
        - address:
        This address is the 128-bit representation of the IPv6 address.
        It is assumed that it is in network byte ordering.
        - portNumber:
        This is the port number in host byte order.  The port number zero is
        assumed.
    */
    init(address: in6_addr, portNumber: Int = 0, scope: Int = 0) {
        sin6_len = UInt8(sizeof(sockaddr_in6))
        sin6_family = UInt8(AF_INET6)
        sin6_port = UInt16(portNumber).bigEndian
        sin6_addr = address
        sin6_flowinfo = 0 // This field seems to be obsolete.
        sin6_scope_id = UInt32(scope)
    }
}

/**
Get a NetworkAddress instance from a **NSData** containing a legacy C-based
structure such as **sockaddr_in**, **sockaddr_in6**, or **sockaddr_storage**
containing one of the former structures.  This function does the minimum
required to determine the family of the structure, then dispatches the result
to the appropriate NetworkAddress specialized for the family, such as
**IPv4NetworkAddress**, or **IPv6NetworkAddress**.

- Parameters:
    - data:
    **NSData** containing one of the following structures: **sockaddr_in**,
**sockaddr_in6**, or **sockaddr_storage**.
- Throws: InvalidAddress
This error is thrown if the bytes stored by the *storage* parameter don't
represent a valid **sockaddr_in**, **sockaddr_in6**, or **sockadd_storage**
structure
- Throws: WrongFamily
This error indicates that the storage structure passed to this initalizer
represents an address that hasn't been implemented, such as **AF_PPP**
*/
func getNetworkAddress(sockaddr data: NSData) throws -> NetworkAddress {
    // We only need the first two bytes to check the family type
    guard data.length >= 2 else { throw NetAddressError.InvalidAddress }
    
    // Get a reference to the structure as a sockaddr_storage
    let storageIn = getSockStorage(data.bytes, data.length)

    // Let the specializations of NetworkAddress for IPv4 and IPv6 handle
    // creating themselves from the provided struct.  It is possible for
    // them to detect more errors, so if they throw errors, re-throw them.
    if storageIn.ss_family == sa_family_t(AF_INET) {
        do { return try IPv4NetworkAddress(storage: data) }
        catch let error { throw error }
    }
    
    if storageIn.ss_family == sa_family_t(AF_INET6) {
        do { return try IPv6NetworkAddress(storage: data) }
        catch let error { throw error }
    }
    
    throw NetAddressError.WrongFamily
}

/**
Get a NetworkAddress instance from a **String** containing a domain name.  It
is possible that it will return a NetworkAddress specialization for IPv4 or
IPv6, depending on what the domain name system returns to us.

- Parameters:
    - hostName:
    **String** Containing the domain name for the query
- Throws: HostNotFound
If the host cannot be found, this error will be thrown.
*/
func getNetworkAddress(hostName: String,
    serviceName: String? = nil,
    proto: Int32 = IPPROTO_TCP,
    sockType: Int32 = SOCK_STREAM) throws -> [NetworkAddress] {
    
    var hints = addrinfo(
        ai_flags: AI_ADDRCONFIG, // We only want IPs that we can access
        ai_family: PF_UNSPEC,
        ai_socktype: sockType,
        ai_protocol: proto,
        ai_addrlen: 0,
        ai_canonname: nil,
        ai_addr: nil,
        ai_next: nil)
    var results: UnsafeMutablePointer<addrinfo> = nil
    
    let ret: Int32
    if serviceName != nil {
        ret = getaddrinfo(hostName, serviceName!, &hints, &results)
    } else {
        ret = getaddrinfo(hostName, nil, &hints, &results)
    }

    guard ret == 0 else {
        let errorString = String(UTF8String: gai_strerror(ret))
        OSULoggerLog(.Warning, string:
            "Unable to resolve hostname \(hostName): \(errorString)")
        throw NetAddressError.KernelError(ret)
    }
    
    // Ensure that the first result is non-nil.  It shouldn't be, but we force-
    // unwrap in the first iteration through the loop.
    var result: addrinfo? = results.memory
    guard result != nil else { throw NetAddressError.HostNotFound }

    var addresses = [NetworkAddress]()
    repeat {
        let data = NSData(bytes: result!.ai_addr, length: Int(result!.ai_addrlen))
        if let addr = try? getNetworkAddress(sockaddr: data) {
            addresses.append(addr)
            OSULoggerLog(.Debugging, string:
                "Found new address for \(hostName): \(addr.address). " +
                " Protocol: \(result!.ai_protocol)," +
                " sock type: \(result!.ai_socktype)")
        } else {
            OSULoggerLog(.Information, string:
                "Unable to create address structure from DNS result.")
        }
    
        if result!.ai_next != nil {
            result = result!.ai_next.memory
        } else {
            result = nil
        }
    } while result != nil
    
    return addresses
}

public func getNetworkAddress(sock: Int32) throws -> NetworkAddress {
    // Create an empty sockaddr_storage structure to hold the return
    var sockSize = socklen_t(sizeof(sockaddr))
    var retSockAddr = sockaddr_storage()

    // Get the bound address from the socket
    guard getSockNameStorage(sock, &retSockAddr, &sockSize) != -1 else {
        OSULoggerLog(.Warning, string: "Unable to get port number from socket.")
        throw OSUBenchError.KernelError(errno)
    }
    
    // Create a new NetworkAddress
    do {
        let data = NSData(bytes: &retSockAddr, length: Int(sockSize))
        return try getNetworkAddress(sockaddr: data)
    } catch let error {
        throw error
    }
}

/**
The NetworkAddress protocol is used to provide a consistent interface for
storing and manipulating network addresses of either IPv4 or IPv6 types.

As such, the address variable is presented and consumed in String form.
*/
public protocol NetworkAddress : CustomStringConvertible, CustomDebugStringConvertible {
    var address:     String  { get set }
    var netmask:     String? { get }
    var portNumber:  Int     { get set }
    var hostName:    String? { get }
    var family:      Int32   { get }
    
    var hashValue:   Int     { get }
    
    static func anyAddress(portNumber: Int) -> NetworkAddress
    
    func getAddrStructStorage() throws -> sockaddr_storage
    func getNSData() throws -> NSData
    func accessible(address: NetworkAddress) throws -> Bool
}

public struct IPv4NetworkAddress : NetworkAddress, Hashable {
    private var _address:  String
    private var _hostName: String? = nil
    private var _netmask:  String? = nil
    private var _netmask_addr: in_addr_t? = nil

    public var family: Int32 { get { return AF_INET } }
    
    public var portNumber: Int = 0
    public var address: String {
        get { return _address }
        set {
            _address = newValue
            // Invalidate the existing hostname.
            // It will be recreated later if requested.
            _hostName = nil
        }
    }
    
    public var hostName: String? {
        get {
            do {
                var addrStructData = try self.getAddrStruct()
                let hostEntPtr = gethostbyaddr(
                    &addrStructData.sin_addr, socklen_t(sizeof(in_addr)), AF_INET)
                // If that came back nil, we don't have anything to work with.
                guard hostEntPtr != nil else { return nil }
                let hostEnt = hostEntPtr.memory
                if let str = String(UTF8String: hostEnt.h_name) { return str }
                else { return _address }
            } catch { return nil }
        }
    }
    
    public var netmask: String? {
        get { return _netmask }
        set {
            _netmask = newValue
            _netmask_addr = nil
        }
    }
    
    public var netmask_addr: in_addr_t? {
        set {
            var val = newValue
            var addrCString = Array<Int8>(count: Int(INET_ADDRSTRLEN), repeatedValue: 0)
            let ret = inet_ntop(AF_INET, &val, &addrCString, socklen_t(INET_ADDRSTRLEN))
            if ret != nil, let str = String(UTF8String: UnsafePointer<CChar>(addrCString)) {
                _netmask = str
            } else {
                _netmask = nil
            }
        }
        
        get {
            guard _netmask != nil else { return nil }
            var inAddr = in_addr()
            let ret = inet_pton(AF_INET, _netmask!, &inAddr)
            guard ret == 1 else { return nil }
            return inAddr.s_addr
        }
    }
    
    public var hashValue: Int {
        get {
            return _address.hashValue
        }
    }
    
#if OSUNETWORK_JSON_SUPPORT
    public var jsonRep: JSON {
        get {
            var outputDict = [String: JSON]()
            
            outputDict["protocol"] = JSON.String("IPv4")
            outputDict["address"]  = JSON.String(_address)
            if portNumber != 0 { outputDict["port"] = JSON.Int64(portNumber) }
            if let snm = _netmask { outputDict["netmask"] = JSON.String(snm) }
            
            return JSON(outputDict)
        }
    }
#endif

    // This is one of the primary points that need to be different
    // between IPv4 and IPv6.  They both have to implement this
    // function, though the only real difference is the AF_INET? field
    // and the creation of the appropriate struct
    func getAddrStruct() throws -> sockaddr_in {
        // Get the in_addr structure for the given address
        var inAddr = in_addr()
        let ret = inet_pton(AF_INET, _address, &inAddr)
        switch ret {
        case 0:
            throw NetAddressError.InvalidAddress
        case -1:
            throw NetAddressError.KernelError(errno)
        default:
            return sockaddr_in(address: inAddr.s_addr, portNumber: portNumber)
        }
    }
    
    public func getAddrStructStorage() throws -> sockaddr_storage {
        do {
            return getSockFromIn(try self.getAddrStruct())
        } catch let error {
            throw error
        }
    }
    
    public func getNSData() throws -> NSData {
        do {
            var sockStruct = try self.getAddrStruct()
            let data = NSData(bytes: &sockStruct, length: sizeof(sockaddr_in))
            return data
        } catch let error {
            throw error
        }
    }
    
    public static func anyAddress(portNumber: Int = 0) -> NetworkAddress {
        return IPv4NetworkAddress(address: "0.0.0.0", portNumber: portNumber)
    }
    
    public init(address: String, portNumber: Int = 0) {
        _address = address
        self.portNumber = portNumber
    }
    
    init(storage: NSData) throws {
        do {
            // Have the sockaddr_in extension do all the hard work.
            let addr = try sockaddr_in(storage: storage)

            // Convert the numeric address into a string for storage
            let addressUTF8 = inet_ntoa(addr.sin_addr)
            let addressStr  = String(
                CString: addressUTF8,
                encoding: NSUTF8StringEncoding)
            guard addressStr != nil else {
                throw NetAddressError.InvalidAddress
            }
            _address = addressStr!

            // The portnumber in the structure is stored in big endian.
            // If necessary, convert that to host byte order.
            portNumber = Int(CFSwapInt16BigToHost(addr.sin_port))

        } catch let error {
            throw error
        }
    }
    
    /**
    The network number is the address AND'd with the netmask.  It's basically
    the lowest IP address that's within the subnet.
    */
    func networkNumber() throws -> in_addr_t {
        guard _netmask != nil else { throw NetAddressError.NetmaskMissing }
        do {
            let addrStruct = try getAddrStruct()
            guard let netmaskAddr = self.netmask_addr else {
                throw NetAddressError.InvalidAddress
            }
            return addrStruct.sin_addr.s_addr | netmaskAddr
        } catch let error {
            throw error
        }
    }
    
    /**
    Determine whether the provided address is accessible (without routing) from
    this address.  This address must have a netmask set.
    */
    public func accessible(testAddress: NetworkAddress) throws -> Bool {
        // If the given address is IPv6 we'll say that it's not accessible
        // from this IPv4 network.
        if testAddress.dynamicType != IPv4NetworkAddress.self { return false }
        
        // The "network number" of the test address with this netmask should
        // equal the network address of the network itself if the given address
        // is accessible from this network
        do {
            let testNetwork = try (testAddress as! IPv4NetworkAddress).networkNumber()
            let thisNetwork = try self.networkNumber()
            return testNetwork == thisNetwork
        } catch let error { throw error }
    }
    
    public var debugDescription: String { get { return self.description } }
    
    public var description: String {
        get {
            return "IPv4 address: \(_address): \(portNumber)"
        }
    }
}

public struct IPv6NetworkAddress : NetworkAddress {
    // Internal storage for the variables
    var _address: String
    var _portnum: Int = 0
    var _scopeID: Scope = .NullReserved
    var _hostName: String? = nil
    
    public var netmask: String? {
        get {
            // An IPv6 subnet is just the upper-half of the IPv6 address
            do {
                // Get the address struct
                var addrStruct = isolateIn6Subnet(try self.getAddrStruct())
                
                var addrCString = Array<Int8>(
                    count: Int(INET6_ADDRSTRLEN), repeatedValue: 0)

                let ret = inet_ntop(
                    AF_INET6,
                    &addrStruct,
                    &addrCString,
                    socklen_t(INET6_ADDRSTRLEN))

                guard ret != nil else { return nil }

                if let str = String(UTF8String: UnsafePointer<CChar>(addrCString)) {
                    return str

                } else { return nil }
            } catch { return nil }
        }
    }
    
    public var family: Int32 { get { return AF_INET6 } }
    
    public enum Scope {
        case NullReserved
        case InterfaceLocal
        case LinkLocal
        case AdminLocal
        case SiteLocal
        case OrgLocal
        case Global
        case Reserved
    }

    private func rawValueFromScope(input: Scope) throws -> Int {
        switch input {
        case .NullReserved:   return 0x00
        case .InterfaceLocal: return 0x01
        case .LinkLocal:      return 0x02
        case .AdminLocal:     return 0x04
        case .SiteLocal:      return 0x05
        case .OrgLocal:       return 0x08
        case .Global:         return 0x0e
        case .Reserved:       return 0x0f
        }
    }
    
    private func scopeFromRaw(input: Int) throws -> Scope {
        switch (input & 0x0F) {
        case 0x00: return .NullReserved
        case 0x01: return .InterfaceLocal
        case 0x02: return .LinkLocal
        case 0x04: return .AdminLocal
        case 0x05: return .SiteLocal
        case 0x08: return .OrgLocal
        case 0x0e: return .Global
        case 0x0f: return .Reserved
        default: throw NetAddressError.InvalidScope
        }
    }

    public var address:     String {
        get { return _address }
        set {
            _address = newValue
            _hostName = nil
        }
    }
    
    public var portNumber:  Int {
        get { return _portnum }
        set { _portnum = newValue }
    }
    
    public var scopeID: Scope {
        get { return _scopeID }
        set { _scopeID = newValue }
    }
    
    public var hashValue: Int {
        get {
            return _address.hashValue
        }
    }
    
#if OSUNETWORK_JSON_SUPPORT
    public var jsonRep: JSON {
        get {
            var outputDict = [String: JSON]()
            
            outputDict["protocol"] = "IPv6"
            outputDict["address"] = JSON(_address)
            if portNumber != 0   { outputDict["port"]    = JSON(portNumber) }
            if let snm = netmask { outputDict["netmask"] = JSON(snm) }
            
            return JSON(outputDict)
        }
    }
#endif

    // This is one of the primary points that need to be different
    // between IPv4 and IPv6.  They both have to implement this
    // function and return the sockaddr_storage struct.  There are
    // also places where it would be convenient to move back and
    // forth between the specific structs for each protocol.
    public func getAddrStructStorage() throws -> sockaddr_storage {
        do {
            return getSockFromIn6(try self.getAddrStruct())
        } catch let error {
            throw error
        }
    }
    
    public func getNSData() throws -> NSData {
        do {
            var sockStruct = try self.getAddrStruct()
            let data = NSData(bytes: &sockStruct, length: sizeof(sockaddr_in6))
            return data
        } catch let error {
            throw error
        }
    }

    // This is one of the primary points that need to be different
    // between IPv4 and IPv6.  They both have to implement this
    // function, though the only real difference is the AF_INET? field
    // and the creation of the appropriate struct
    public func getAddrStruct() throws -> sockaddr_in6 {
        // Get the in_addr structure for the given address
        var addrStruct = in6_addr()
        let ret = inet_pton(AF_INET6, _address, &addrStruct)
        switch ret {
        case 0:
            throw NetAddressError.InvalidAddress
        case -1:
            throw NetAddressError.KernelError(errno)
        default:
            do {
                return sockaddr_in6(
                    address: addrStruct,
                    portNumber: _portnum,
                    scope: try rawValueFromScope(_scopeID)
                )
            } catch let error {
                throw error
            }
        }
    }
    
    // Creates an IPv6 address from a string.  The scope and port number are optional
    public init(address: String, portNumber: Int = 0, scope: Scope = .NullReserved) {
        _address = address
        _portnum = portNumber
        _scopeID = scope
    }
    
    public init(storage: NSData) throws {
        // Ensure that the size of the data is enough
        guard storage.length >= sizeof(sockaddr_in6) else {
            throw OSUNetResolverError.InvalidAddress
        }
        
        var structIn = getSockIn6(storage.bytes, storage.length)
        
        // Ensure that we're looking at the correct address type
        guard structIn.sin6_family == sa_family_t(AF_INET6) else {
            throw OSUNetResolverError.InvalidAddress
        }
        
        // Copy the address into a string
        var addrCString = Array<Int8>(count: Int(INET6_ADDRSTRLEN), repeatedValue: 0)
        let ret = inet_ntop(
            AF_INET6, &structIn.sin6_addr,
            &addrCString, socklen_t(INET6_ADDRSTRLEN))
        if ret == nil { throw NetAddressError.KernelError(errno) }
        if let str = String(UTF8String: UnsafePointer<CChar>(addrCString)) {
            _address = str
        } else {
            throw OSUNetResolverError.InvalidAddress
        }

        // The local portnum is stored in host order
        _portnum = Int(CFSwapInt16BigToHost(structIn.sin6_port))

        // Copy the scope ID
        do { _scopeID = try scopeFromRaw(Int(structIn.sin6_scope_id)) }
        catch let error { throw error }
    }

    // Creates an IPv6 address suitable for listening from any address
    public static func anyAddress(portNumber: Int = 0) -> NetworkAddress {
        let addrStruct = in6_addr()
        var sockStruct = sockaddr_in6(address: addrStruct, portNumber: portNumber)
        let data = NSData(bytes: &sockStruct, length: sizeof(sockaddr_in6))
        return try! IPv6NetworkAddress(storage: data)
    }

    public var hostName: String? { get {
        // Only do work if we have to
        if _hostName != nil { return _hostName }
        
        do {
            var hostCString = Array<Int8>(count: Int(NI_MAXHOST), repeatedValue: 0)
            var servCString = Array<Int8>(count: Int(NI_MAXSERV), repeatedValue: 0)
            var sockaddrStruct = getSockaddrFromStorage(try self.getAddrStructStorage())
            let ret = getnameinfo(
                &sockaddrStruct, socklen_t(sockaddrStruct.sa_len),
                &hostCString, socklen_t(NI_MAXHOST),
                &servCString, socklen_t(NI_MAXSERV), 0)
            if ret != 0 {
                return nil
            }
            
            if let str = String(UTF8String: hostCString) {
                return str
            }
        } catch {
            return nil
        }
        
        // If we can't get the hostname, use the IP instead
        return nil
    } }

    /**
    Determine whether the provided address is accessible (without routing) from
    this address.
     */
    public func accessible(testAddress: NetworkAddress) throws -> Bool {
        // If the given address is IPv4 we'll say that it's not accessible
        // from this IPv6 network.
        if testAddress.dynamicType != IPv6NetworkAddress.self { return false }
        
        // In IPv6, the higher-order 64 bits of the address are the routing
        // prefix and subnet id.  It's as if there's a constant 64-bit subnetmask.

        // The following is necessary because the in6_addr is a union, and it
        // appears that Swift doesn't know what to do with that.  Therefore, it
        // should be safe to bitcast that into a array of 4 32-bit unsigned integers.
        do {
            let lhsStr = try self.getAddrStruct()
            let rhsStrData = try testAddress.getNSData()
            let rhsStr = getSockIn6(rhsStrData.bytes, rhsStrData.length)
            let result = compareIn6Subnet(lhsStr.sin6_addr, rhsStr.sin6_addr)
            if result == 0 { return true } else { return false }
        } catch let error { throw error }
    }

    public var debugDescription: String { get { return self.description } }

    public var description: String {
        get {
            return "IPv6 address: \(_address):\(_portnum)"
        }
    }
}

extension in_addr:  Equatable {}
extension in6_addr: Equatable {}

public func == (lhs: in_addr, rhs: in_addr) -> Bool {
    return lhs.s_addr == rhs.s_addr
}

public func == (lhs: in6_addr, rhs: in6_addr) -> Bool {
    let result = compareIn6Addr(lhs, rhs)
    if result == 0 { return true } else { return false }
}

// IPv4 compare
public func == (lhs: IPv4NetworkAddress, rhs: IPv4NetworkAddress) -> Bool {
    do {
        let v4lhs = try lhs.getAddrStruct()
        let v4rhs = try rhs.getAddrStruct()
        if v4lhs.sin_port != v4rhs.sin_port { return false }
        if v4lhs.sin_addr != v4rhs.sin_addr { return false }
        return true
    } catch {
        return false
    }
}

// IPv6 compare
public func == (lhs: IPv6NetworkAddress, rhs: IPv6NetworkAddress) -> Bool {
    do {
        let v6lhs = try lhs.getAddrStruct()
        let v6rhs = try rhs.getAddrStruct()
        if v6lhs.sin6_port     != v6rhs.sin6_port     { return false }
        if v6lhs.sin6_addr     != v6rhs.sin6_addr     { return false }
        if v6lhs.sin6_flowinfo != v6rhs.sin6_flowinfo { return false }
        if v6lhs.sin6_scope_id != v6rhs.sin6_scope_id { return false }
        return true
    } catch {
        return false
    }
}

public func ==(lhs: NetworkAddress, rhs: NetworkAddress) -> Bool {
    // Only addresses of the same protocol are considered equal.
    guard lhs.dynamicType == rhs.dynamicType else {
        return false
    }
    
    if lhs.dynamicType == IPv4NetworkAddress.self {
        return (lhs as! IPv4NetworkAddress) == (rhs as! IPv4NetworkAddress)
    }

    if lhs.dynamicType == IPv6NetworkAddress.self {
        return (lhs as! IPv6NetworkAddress) == (rhs as! IPv6NetworkAddress)
    }
    
    return false
}

public func !=(lhs: NetworkAddress, rhs: NetworkAddress) -> Bool {
    return !(lhs == rhs)
}
