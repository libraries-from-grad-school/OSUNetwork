//
//  OSUNetSession_Tests.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import XCTest
@testable import OSUNetwork

class OSUNetSession_Tests: XCTestCase, OSUNetSessionDelegate {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testSerializeString() {
        do {
            let session = OSUNetSession()
            let sampleString: String = "😈ℑ∞♔ᾏἊHello1"
            let sampleStringData = try session.processString(sampleString)
            let exemplarData = NSData(bytes: stringData1, length: stringData1.count)
            // Unfortunately, there are tons or ways to encode unicode strings
            // so we can't actually just compare bytes.  We actually have to make
            // new strings and use the string functions to compare them.
            if  let sS = NSString(data: sampleStringData, encoding: NSUTF16StringEncoding),
                let eS = NSString(data: exemplarData, encoding: NSUTF16StringEncoding) {
                    assert(sS.compare(eS as String) == NSComparisonResult.OrderedSame, "Strings do not match")
            } else {
                assert(false, "The string processing failed somewhere.")
            }
        } catch {
            assert(false, "The string processing threw an error.")
        }
    }
    
    func testProcessBytesData() {
        // Create an instance of the OSUNetSession class that can be used for testing
        let session = OSUNetSession()
        session.binaryMode = true
        session.delegate = self
        // Set the dispatch queue
        session.delegateDispatchQueue = testQueue
        
        // We'll use the same byte buffers that are used for strings, because
        // they're already all built.
        session.processBytes(UnsafePointer<UInt8>(stringData1), length: stringData1.count)
        // Do the test on the same serial queue that we used for the delegate
        // methods.  Hopefully this will ensure that the processBytes method it
        // done with its work.
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData1, length: stringData1.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        session.processBytes(UnsafePointer<UInt8>(stringData2), length: stringData2.count)
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData2, length: stringData2.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        session.processBytes(UnsafePointer<UInt8>(stringData3), length: stringData3.count)
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData3, length: stringData3.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        session.processBytes(UnsafePointer<UInt8>(stringData4), length: stringData4.count)
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData4, length: stringData4.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        session.processBytes(UnsafePointer<UInt8>(stringData5), length: stringData5.count)
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData5, length: stringData5.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        session.processBytes(UnsafePointer<UInt8>(stringData6), length: stringData6.count)
        dispatch_sync(testQueue) {
            if let lastMessage = self.messages.last as? NSData {
                let testData = NSData(bytes: stringData6, length: stringData6.count)
                assert(testData.isEqualToData(lastMessage) == true,
                    "The returned data doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
    }
    
    func testProcessBytesStrings() {
        // This is the sample string that all the tests will be compared with
        let sampleString: String = "😈ℑ∞♔ᾏἊHello"
        // Create an instance of the OSUNetSession class that can be used for testing
        let session = OSUNetSession()//inputStream: nil, outputStream: nil, name: "Testing")
        session.binaryMode = false
        session.delegate = self
        // Set the dispatch queue
        session.delegateDispatchQueue = testQueue
        // Perform the string methods
        session.processBytes(UnsafePointer<UInt8>(stringData1), length: stringData1.count)
        // Do the test on the same serial queue that we used for the delegate
        // methods.  Hopefully this will ensure that the processBytes method it
        // done with its work.
        dispatch_sync(testQueue) {
            if let messageString = self.messages.last as? String {
                let testString = sampleString + "1"
                assert(testString.compare(messageString) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        // The next test processes two strings contained within one buffer.
        // the result of which should be two invocations of the receieved message
        // call, and two distinct string.
        session.processBytes(UnsafePointer<UInt8>(stringData2), length: stringData2.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 3,
                "ProcessBytes failed to produce the correct number of messages")
            let testString2 = sampleString + "2"
            if let messageString = self.messages[1] as? String {
                assert(messageString.compare(testString2) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
            let testString3 = sampleString + "3"
            if let messageString = self.messages[2] as? String {
                assert(messageString.compare(testString3) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        // This test contains only a partial string.  It should not produce
        // an invocation of the delegate method.  It should, however, keep the
        // partial data and prepend it to the following message
        session.processBytes(UnsafePointer<UInt8>(stringData3), length: stringData3.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 3,
                "ProcessBytes produced a message when it shouldn't have,") }
        
        // This next test will complete the previous string, and provide another string on its own.
        // the expected result is two invocations of the delegate method.
        session.processBytes(UnsafePointer<UInt8>(stringData4), length: stringData4.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 5,
                "ProcessBytes failed to produce the correct number of messages")
            let testString4 = sampleString + "4"
            if let messageString = self.messages[3] as? String {
                assert(messageString.compare(testString4) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
            let testString5 = sampleString + "5"
            if let messageString = self.messages[4] as? String {
                assert(messageString.compare(testString5) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        // This test requires a partial string, then another string, with a partial
        session.processBytes(UnsafePointer<UInt8>(stringData3), length: stringData3.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 5,
                "ProcessBytes produced a message when it shouldn't have,") }
        session.processBytes(UnsafePointer<UInt8>(stringData5), length: stringData5.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 7,
                "ProcessBytes failed to produce the correct number of messages")
            let testString6 = sampleString + "6"
            if let messageString = self.messages[5] as? String {
                assert(messageString.compare(testString6) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
            let testString7 = sampleString + "7"
            if let messageString = self.messages[6] as? String {
                assert(messageString.compare(testString7) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        // This test requires a partial string, then
        // completes it with the remainder of the partial
        session.processBytes(UnsafePointer<UInt8>(stringData6), length: stringData6.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 8,
                "ProcessBytes failed to produce the correct number of messages")
            let testString7 = sampleString + "8"
            if let messageString = self.messages[7] as? String {
                assert(messageString.compare(testString7) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
        
        // Finally, perform the first test again to make sure
        // that there wasn't any remainder kicking around
        session.processBytes(UnsafePointer<UInt8>(stringData1), length: stringData1.count)
        dispatch_sync(testQueue) {
            assert(self.messages.count == 9,
                "ProcessBytes failed to produce the correct number of messages")
            let testString8 = sampleString + "1"
            if let messageString = self.messages[8] as? String {
                assert(messageString.compare(testString8) == .OrderedSame,
                    "The returned string doesn't match the provided one.")
            } else {
                assert(false,
                    "OSUNetSession failed to produce a message when it should have.")
            }
        }
    }
    
    let testQueue = dispatch_queue_create("TestingQueue", DISPATCH_QUEUE_SERIAL)
    var messages = [AnyObject]()
    func netSession(session: OSUNetSession, receivedMessage: AnyObject) {
        messages.append(receivedMessage)
    }
    
    func netSession(session: OSUNetSession, errorOccured: OSUNetSessionError) {
        assert(false, "Net session error occured.")
    }
    
    func netSessionClosed(session: OSUNetSession) {
        assert(false, "Net session closed.")
    }    
}

// All the string data is presented as UInt8s so that we can control it at
// the byte level.  The goal is that we test all the the possilbities for inputs
// First, we'll have a simple string that contains exactly one string (with some
// unicode in there for fun
let stringData1: [UInt8] = [
    0xd8, 0x3d, 0xde, 0x08, 0x21, 0x11, 0x22, 0x1e, 0x26,
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x31, 0x00, 0x00
]
// The next string contains two strings precisely
let stringData2: [UInt8] = [
    0xd8, 0x3d, 0xde, 0x08, 0x21, 0x11, 0x22, 0x1e, 0x26,
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x32, 0x00, 0x00,
    0xd8, 0x3d, 0xde, 0x08, 0x21, 0x11, 0x22, 0x1e, 0x26,
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x33, 0x00, 0x00
]
// This string contains only a partial string
let stringData3: [UInt8] = [
    0xd8, 0x3d, 0xde, 0x8, 0x21, 0x11, 0x22, 0x1e, 0x26
]
// This string contains a partial string (to be conbined with the prior)
// and a whole string
let stringData4: [UInt8] = [
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x34, 0x00, 0x00,
    0xd8, 0x3d, 0xde, 0x08, 0x21, 0x11, 0x22, 0x1e, 0x26,
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x35, 0x00, 0x00
]
// This string contains a partial string (to be conbined with the prior),
// another whole string, and another partial
let stringData5: [UInt8] = [
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x36, 0x00, 0x00,
    0xd8, 0x3d, 0xde, 0x08, 0x21, 0x11, 0x22, 0x1e, 0x26,
    0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00, 0x48, 0x00, 0x65,
    0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x37, 0x00, 0x00,
    0xd8, 0x3d, 0xde, 0x8, 0x21, 0x11
]
// This contains the terminal part of the partial starting string
let stringData6: [UInt8] = [
    0x22, 0x1e, 0x26, 0x54, 0x1f, 0x8f, 0x1f, 0x0a, 0x00,
    0x48, 0x00, 0x65, 0x00, 0x6c, 0x00, 0x6c, 0x00, 0x6f,
    0x00, 0x38, 0x00, 0x00
]
