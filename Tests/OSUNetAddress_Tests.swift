//
//  OSUNetAddress_Tests.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import XCTest
@testable import OSUNetwork

class OSUNetAddress_Tests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after
        // the invocation of each test method in the class.
    }

    func testNetworkAddressIPv4() {
        do {
            // Create a NetworkAddress from a string
            let stringAddress = IPv4NetworkAddress(
                address: "192.168.0.1", portNumber: 12345)
            
            // Create a sockaddr_in from the address
            let sockData = try stringAddress.getNSData()
            
            // Make sure that the resulting structure is objectively correct
            // this is done by using the hand-converted, expected, results
            let storageIn = getSockIn(sockData.bytes, sockData.length)
            assert(storageIn.sin_port == UInt16(12345).bigEndian,
                   "Conversion of address into sockaddr_in produced incorrect output.")
            assert(storageIn.sin_addr.s_addr == in_addr_t(0x0100a8c0),
                   "Conversion of address into sockaddr_in produced incorrect output.")
            
            // Create a new NetworkAddress from the struct (just to make sure it doesn't fail)
            let _ = try IPv4NetworkAddress(storage: sockData)
            
            // Try to detect that this is an IPv4 address
            let ipv4 = try getNetworkAddress(sockaddr: sockData)
            assert(ipv4.dynamicType == IPv4NetworkAddress.self,
                "getNetworkAddress didn't return an address with the correct address family.")
            
        } catch let error {
            assert(false,
                "Failed to create a NetworkAddress from a sockaddr_storage. \(error)")
        }
    }
    
    func testHostNameGoogle() {
        do {
            let addresses = try getNetworkAddress("google.com")
            assert(addresses.count > 0, "Unable to load google.com")
        } catch let error {
            assert(false, "Error occured while attempting to resolve google.com \(error)")
        }

        do {
            let addresses = try getNetworkAddress("google.com", serviceName: "http")
            assert(addresses.count > 0, "Unable to load google.com")
        } catch let error {
            assert(false, "Error occured while attempting to resolve google.com \(error)")
        }
    }
/* This test is intended to test whether a mDNS system is up and functional,
   but, I don't know a good way to get the local mDNS host name that could be
   used in a generic sense to perform this test.  Feel free to replace the string
   constants with your own mDNS name and check whether this test passes.
    func testHostNameLocal() {
        do {
            let addresses1 = try getNetworkAddress("Falcon.local")
            assert(addresses1.count > 0, "Unable to load Falcon.local addresses")

            let addresses2 = try getNetworkAddress("Falcon.local", serviceName: "http")
            assert(addresses2.count > 0, "Unable to load Falcon.local address for http")
            
            // We should get the same number of addresses in each case
            assert(addresses1.count == addresses2.count, "Receieved an inequal number of addresses")
            
            // Compare the addresses from each array.  We should find the same
            // number of matches than there are addresses.
            for var lhs in addresses1 {
                // The first addresses don't have the port number set, so they
                // won't match the second addresses.  Set it now so that we can
                // test address equality.
                lhs.portNumber = 80
                var match = false
                for rhs in addresses2 {
                    if lhs == rhs {
                        match = true
                        break
                    }
                }
                
                assert(match == true, "Found an address that isn't matched (probably a problem in an == function.")
            }
        } catch let error {
            assert(false, "Error occured while attempting to resolve localhost.local \(error)")
        }
    }
*/
}
