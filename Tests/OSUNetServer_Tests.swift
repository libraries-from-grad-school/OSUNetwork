//
//  OSUNetServer_Tests.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import XCTest
import OSULogger
@testable import OSUNetwork

var hostname: String = ""
var succeeded: Bool? = nil

class OSUNetServerTestHarness: OSUNetServer {
    internal override func netServiceDidPublish(sender: NSNetService) {
        succeeded = true
        // Now that the service is published, the name and domain are valid
        name = sender.name
        domain = sender.domain
        OSULoggerLog(.Information, string: "Service published as \(name).\(domain)")
        guard delegate != nil else { return }
        delegate!.published(self)
    }
    
    internal
    override func netService(sender: NSNetService, didNotPublish errorDict: [String : NSNumber]) {
        succeeded = false
        if let errorCode = errorDict[NSNetServicesErrorCode],
            let errorEnum = NSNetServicesError(rawValue: errorCode.integerValue) {
                OSULoggerLog(.Warning, string: "Net service did not publish: \(errorEnum.description)")
        }
    }
}

class TestBrowserDelegate: OSUNetBrowserDelegate {
    func browser(browser: OSUNetBrowser, foundService: NSNetService) {
        if foundService.name == hostname {
            succeeded = true
        }
    }
    
    func browser(browser: OSUNetBrowser, removedService: NSNetService) {
        if removedService.name == hostname {
            succeeded = true
        }
    }
}

class OSUNetServer_Tests: XCTestCase {
    override func setUp() {
        succeeded = nil
    }

    func testServerWithTXT() {
        let server = OSUNetServerTestHarness()
        server.type = "_OSUNetServerTestTXT._tcp."
        server.txt = ["TestTXT" : "Success".dataUsingEncoding(NSUTF8StringEncoding)!]
        
        do {
            try server.start(NSRunLoop.currentRunLoop())
        } catch let error {
            assert(false, "Error thrown in server start: \(error)")
        }
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to publish the net server")
        } else {
            assert(false, "Did not receieve a callback from bonjour")
        }

        OSULoggerLog(.Information, string: server.description)
        
        hostname = server.name
        
        // Change the txt record to be nil
        server.txt = nil
        
        // Attempt to browse for this server
        succeeded = nil
        let delegate = TestBrowserDelegate()
        let browser = OSUNetBrowser()
        browser.delegate = delegate
        browser.search("_OSUNetServerTestTXT._tcp.")
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to discover the net server")
        } else {
            assert(false, "Did not receieve a callback from discovery")
        }

        // Close the server
        succeeded = nil
        server.stop()
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to remove the net server")
        } else {
            assert(false, "Did not receieve a callback from discover")
        }
    }

    func testServerWithoutTXT() {
        let server = OSUNetServerTestHarness()
        server.type = "_OSUNetServerTestNoTXT._tcp."
        server.txt = nil
        
        do {
            try server.start()
        } catch let error {
            assert(false, "Error thrown in server start: \(error)")
        }
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to publish the net server")
        } else {
            assert(false, "Did not receieve a callback from bonjour")
        }

        // Get the domain and name from the server instance
        hostname = server.name
        print("\(server.name).\(server.domain):\(server.port)")
        print(server.txt)
        print(server.type)
        
        // Change the txt record to be non-nil
        server.txt = ["TestTXT" : "Success".dataUsingEncoding(NSUTF8StringEncoding)!]
        
        // Attempt to browse for this server
        succeeded = nil
        let delegate = TestBrowserDelegate()
        let browser = OSUNetBrowser()
        browser.delegate = delegate
        browser.search("_OSUNetServerTestNoTXT._tcp.")
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to discover the net server")
        } else {
            assert(false, "Did not receieve a callback from discovery")
        }
        
        // Close the server
        succeeded = nil
        server.stop()
        
        // Wait up to 5 seconds for callbacks and other ops
        for _ in 0..<5 {
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 1))
            if succeeded != nil { break }
        }
        if let succeeded = succeeded {
            assert(succeeded, "Failed to remove the net server")
        } else {
            assert(false, "Did not receieve a callback from discover")
        }
    }
}