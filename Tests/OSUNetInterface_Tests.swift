//
//  OSUNetInterface_Tests.swift
//  OSUNetwork
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import XCTest
@testable import OSUNetwork

class OSUNetInterface_Tests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testInterfaceEnumeration() {
        // Attempt to get a list of interfaces
        do {
            let interfaces = try OSUNetInterface.getInterfaces()
            
            assert(interfaces.count > 0, "Got an empty list of interfaces")
            
            // Make sure that we got at least one IPv4 and IPv6 address
            var addressFound = false
            for interface in interfaces {
                for address in interface.addresses {
                    if address.dynamicType == IPv6NetworkAddress.self { addressFound = true }
                    if address.dynamicType == IPv4NetworkAddress.self { addressFound = true }
                }
            }
            
            assert(addressFound, "Interface discovery failed to find an address.")
            
        } catch let error {
            assert(true, "getInterfaces threw an error: \(error)")
        }
    }

}
